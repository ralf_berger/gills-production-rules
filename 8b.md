# Exercise 8b

## Production rules

```ruby
# We currently don't have a card inserted. We need to check if
# there is an ejected one in the slot before doing anything else.
if not wm_exists(CARD) and not wm_exists(CARD_CHECKED)
then dm_check(card), wm_set(CARD_CHECKED)

# The machine ejected a card. Could be ours, let's take it and
# note that there isn't an ejected card visibile anymore.
if not wm_exists(CARD) and wm_exists(CARD_CHECKED) and wm_exists(CARD_VISIBLE)
then dm_remove(card), wm_delete(CARD_VISIBLE)

# We currently don't have a card inserted and there isn't
# one ejected. Yay, we can start using the machine.
if not wm_exists(CARD) and wm_exists(CARD_CHECKED) and not wm_exists(CARD_VISIBLE)
then wm_delete(CARD_CHECKED), dm_insert(card), wm_add(CARD)

# We inserted the card but not the bottle. We look at the panel.
if wm_exists(CARD) and not wm_exists(BOTTLE) and not wm_exists(PANEL_CHECKED)
then dm_check(panel), wm_set(PANEL_CHECKED)

# We inserted the card but not the bottle. We watch the panel but it's still closed.
if wm_exists(CARD) and not wm_exists(BOTTLE) and wm_exists(PANEL_CHECKED)
   and wm_exists(PANEL_VISIBLE)
then dm_check(panel)

# We inserted the card but not the bottle. The panel finally opened.
if wm_exists(CARD) and not wm_exists(BOTTLE) and wm_exists(PANEL_CHECKED)
   and not wm_exists(PANEL_VISIBLE)
then wm_unset(PANEL_CHECKED), dm_insert(bottle), wm_set(BOTTLE)

# We inserted the bottle but didn't press the button yet.
if wm_exists(BOTTLE) and not wm_exists(PRESSED)
then dm_press(button_bottle_return), wm_set(PRESSED)

# Waiting for the machine to check the bottle.
if wm_exists(PRESSED) and (not wm_exists(PANEL_CHECKED) or wm_exists(PANEL_VISIBLE))
then dm_check(panel), wm_set(PANEL_CHECKED)

# We looked at the panel, it is opened. Now look for a bottle within the compartment.
if wm_exists(PRESSED) and wm_exists(PANEL_CHECKED) and not wm_exists(BOTTLE_CHECKED)
then dm_check(bottle), wm_set(BOTTLE_CHECKED)

# The panel opened, the bottle disappeared.
if wm_exists(PRESSED) and wm_exists(PANEL_CHECKED)  and not wm_exists(PANEL_VISIBLE)
                      and wm_exists(BOTTLE_CHECKED) and not wm_exists(BOTTLE_VISIBLE)
then wm_delete(BOTTLE), wm_delete(PRESSED),
     dm_press(button_card_return), wm_delete(CARD)

# The panel opened but the bottle is still there!
if wm_exists(PRESSED) and wm_exists(PANEL_CHECKED)  and not wm_exists(PANEL_VISIBLE)
                      and wm_exists(BOTTLE_CHECKED) and     wm_exists(BOTTLE_VISIBLE)
then wm_delete(PRESSED), dm_remove(bottle), wm_delete(BOTTLE)
```


## Working memory entries

* `CARD` -- card inserted and not reachable without ejecting it
* `BOTTLE` -- bottle inserted and being checked by the machine
* `PRESSED` -- the bottle return button has been pressed
* `CARD_VISIBLE` -- information about the card being ejected
* `CARD_CHECKED` -- information about the card being ejected is fresh
* `BOTTLE_VISIBLE` -- information about a bottle visible in the tray
* `BOTTLE_CHECKED` -- information about a bottle visible in the tray is fresh
* `PANEL_VISIBLE` -- information about panel visibility
* `PANEL_CHECKED` -- information about panel visibility is fresh

## Additional transitions and objects

* `dm_check(<dm_new_object>)` -- reads the presence of an object, sets/unsets an (uppercase) working memory entry called `<dm_new_object>_VISIBLE` accordingly.
* `<dm_new_object> :: = panel | <dm_object>`
    * `panel` -- Visible if the machine is closed and bottles can't be inserted.
    * `bottle` -- Is there one visible in the slot to take with you?
    * `card` -- Is it visibly ejected and can be taken out?
